﻿using System;
using System.Collections.Generic;
using System.Text;
using BEZAO_PayDAL.Entities;
using BEZAO_PayDAL.Interfaces.Services;
using BEZAO_PayDAL.Model;
using BEZAO_PayDAL.UnitOfWork;

namespace BEZAO_PayDAL.Services
{
  public  class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Register(RegisterViewModel model)
        {

            if (!Validate(model))
            {
                return;
            }

            var user = new User
            {
                Name = $"{model.FirstName} {model.LastName}",
                Email = model.Email,
                Username = model.Username,
                Birthday = model.Birthday,
                IsActive = true,
                Password = model.ConfirmPassword,
                Account = new Account{AccountNumber = 1209374652},
                Created = DateTime.Now
            };
            _unitOfWork.Users.Add(user);
            _unitOfWork.Commit();
        }

        public void Update(UpdateViewModel model)
        {
            if (!ValidateUpdate(model))
            {
                return;
            }

            var user = _unitOfWork.Users.Find(u => u.Username == model.Username);
            try
            {
                foreach (var u in user)
                {
                    u.Email = model.Email;
                    u.Username = model.Username;
                    u.Password = model.ConfirmNewPassword;
                };
                _unitOfWork.Commit();
                Console.WriteLine("User updated Successfully!");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error: {e.Message}");
            }
            
        }

        public void Login(LoginViewModel model)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            var user = _unitOfWork.Users.Get(id);
            if(user == null){
                Console.WriteLine("Error: User Does not Exist.");
            }
            else
            {
                _unitOfWork.Users.Delete(user);
                _unitOfWork.Commit();
                Console.WriteLine($"{user.Name} Deleted Successfully!");
            }
        }
        public void GetUser(string name)
        {
            
        }
        public void Get(int id)
        {
            var user = _unitOfWork.Users.Get(id);
            if(user == null)
            {
                Console.WriteLine("Error: User does not Exist");
            }
            else { Console.WriteLine($"UserId: {user.Id} Username: {user.Name}"); }
           
        }

        private bool Validate(RegisterViewModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Email) 
                || string.IsNullOrWhiteSpace(model.FirstName) 
                || string.IsNullOrWhiteSpace(model.LastName) 
                || (model.Birthday == new DateTime()) 
                || (string.IsNullOrWhiteSpace(model.Password))
                || (model.Password != model.ConfirmPassword))
            {
                Console.WriteLine("A field is required");
                return false;

            }

            return true;

        }
        private bool ValidateUpdate(UpdateViewModel model)
        {
            if(string.IsNullOrEmpty(model.Email)
                || string.IsNullOrEmpty(model.Username)
                || string.IsNullOrEmpty(model.CurrentPassword)
                || string.IsNullOrEmpty(model.NewPassword)
                || string.IsNullOrEmpty(model.ConfirmNewPassword))
            {
                Console.WriteLine($"{model.Email} Can't be empty.");
                return false;
            }
            return true;
        }
    }
}
