﻿namespace BEZAO_PayDAL.Model
{
    public class WithdrawalViewModel
    {
        public int AccountNumber { get; set; }
        public decimal Amount { get; set; }

    }
}