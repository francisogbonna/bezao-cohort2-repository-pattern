﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEZAO_PayDAL.Entities;
using BEZAO_PayDAL.Interfaces.Repositories;
using BEZAO_PayDAL.Interfaces.Services;
using BEZAO_PayDAL.Model;
using BEZAO_PayDAL.Repositories;
using BEZAO_PayDAL.Services;
using BEZAO_PayDAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;

namespace CodeFirstSoln
{
    partial class Program
    {
        static IUserService userService;
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\tWelcome to Bezao App!");
            Console.ResetColor();
            Console.WriteLine("please choose prefered operation:\n1. Enroll New User\n2.Get view your Details\n3. Delete your account\n4. Perform Transactions ");
            var option = Console.ReadLine();
            switch (option)
            {
                case "1":
                    var enroll = EnrollUser();
                    Console.WriteLine(enroll);
                    Console.ResetColor();
                    break;
                case "2":
                    Console.WriteLine("Please Enter your userID:");
                    int id = int.Parse(Console.ReadLine());
                    try
                    {
                        GetUser(id);
                    }
                    catch (Exception)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Error: Couldn't get user details");
                        Console.ResetColor();
                    }
                    break;
                case "3":
                    Console.WriteLine("Please Enter your userID:");
                    int uId = int.Parse(Console.ReadLine());
                    try
                    {
                        GetUser(uId);
                    }
                    catch (Exception)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Error: Unable to delete user.");
                        Console.ResetColor();
                    }
                    break;
                case "4":
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Ooops! this service is unavailable at the moment please try again.");
                    Console.ResetColor();
                    break;
                default:
                    break;
            }
            Console.ReadLine();
        }

        static string  EnrollUser()
        {
            string msg = null;
            userService = new UserService(new UnitOfWork(new BezaoPayContext()));
            Console.WriteLine("Enter FirstName: ");
            var firstname = Console.ReadLine();
            Console.WriteLine("Enter Last Name:");
            var lastname = Console.ReadLine();
            Console.WriteLine("Enter your Email:");
            var mail = Console.ReadLine();
            Console.WriteLine("Enter username:");
            var username = Console.ReadLine();
            Console.WriteLine("Enter Date of Birth:");
            DateTime dob = DateTime.Parse(Console.ReadLine());
            Console.WriteLine("Enter password:");
            var pass = Console.ReadLine();
            Console.WriteLine("Confirm Password:");
            var confirm = Console.ReadLine();
            if(pass != confirm)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                msg = "Erorr: Password do not match.";
                Console.ResetColor();
                return msg;
            }
            else
            {
                userService.Register(new RegisterViewModel
                {
                    FirstName = firstname,
                    LastName = lastname,
                    Email = mail,
                    Username = username,
                    Birthday = dob,
                    Password = pass,
                    ConfirmPassword = confirm
                });
                Console.ForegroundColor = ConsoleColor.Green;
                msg = "Registration successfull.";
                return msg;
            }

           
            
        }
        static void GetUser(int id)
        {
            userService = new UserService(new UnitOfWork(new BezaoPayContext()));
            userService.Get(id);
        }
        static void Delete(int id)
        {
            userService = new UserService(new UnitOfWork(new BezaoPayContext()));
            userService.Delete(id);
        }
    }
}
